package com.automationpractice.assets;

import com.automationpractice.entities.LoginEntity;

public class LoginAssets {

    public static final String AUTHENTICATION_LBL = "AUTHENTICATION";

    private static final String ACCOUNT_NAME = "Guilherme Tester";

    private static final String VALID_EMAIL = "gui.bue@uninove.edu.br";

    private static final String VALID_PASSWORD = "gui.tester";

    public static LoginEntity getValidLogin() {
        return new LoginEntity(ACCOUNT_NAME, VALID_EMAIL, VALID_PASSWORD);
    }
}
