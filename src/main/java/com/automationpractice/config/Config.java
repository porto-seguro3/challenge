package com.automationpractice.config;

import com.automationpractice.utils.PropertiesUtils;

import java.io.*;
import java.util.Properties;

public class Config {

    private static final File CONFIG_FILE = new File("src/test/resources/config.properties");

    private static Properties properties;

    public Config() {
        throw new IllegalStateException("Utility class");
    }

    protected static Properties getProperties() {
        if (properties != null) {
            return properties;
        }
        properties = PropertiesUtils.getProperties(CONFIG_FILE);
        return properties;
    }
}
