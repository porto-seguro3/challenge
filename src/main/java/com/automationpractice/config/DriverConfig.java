package com.automationpractice.config;

import com.automationpractice.enums.BrowserEnum;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Hello world!
 */
public class DriverConfig {

    private static WebDriver webDriver;

    private static ChromeOptions chromeOptions;

    private DriverConfig() {
        throw new IllegalStateException("Utility class");
    }

    public static void setChromeOptions(ChromeOptions chromeOptions) {
        DriverConfig.chromeOptions = chromeOptions;
    }

    public static ChromeOptions getChromeOptions() {
        if (chromeOptions != null) {
            return chromeOptions;
        }
        chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");
        return chromeOptions;
    }

    private static WebDriver initDriver() {
        String broswer = Config.getProperties().getProperty("driver.broswer", "CHROME");
        switch (BrowserEnum.valueOf(broswer)) {
            case CHORME:
                String pathDriver = Config.getProperties().getProperty("driver.path");
                System.setProperty("webdriver.chrome.driver", pathDriver);
                return new ChromeDriver(getChromeOptions());
            default:
                throw new IllegalStateException("Unexpected value: " + broswer);
        }
    }

    public static WebDriver getWebDriver() {
        if (webDriver == null) {
            webDriver = initDriver();
        }
        return webDriver;
    }

    public static WebDriver setWebDriver(String windowHandle) {
        webDriver = getWebDriver().switchTo().window(windowHandle);
        return webDriver;
    }

    public static WebDriver setWebDriver(WindowType windowType) {
        webDriver = getWebDriver().switchTo().newWindow(windowType);
        return webDriver;
    }

    public static void closeWebDriver() {
        if (webDriver != null) {
            webDriver.close();
            webDriver.quit();
            webDriver = null;
        }
    }

    public static String getUrl(String webApplicationName) {
        return Config.getProperties().getProperty(String.format("url.%s", webApplicationName));
    }
}
