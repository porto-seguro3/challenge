package com.automationpractice.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginEntity {

    private String name;

    private String email;

    private String password;

}
