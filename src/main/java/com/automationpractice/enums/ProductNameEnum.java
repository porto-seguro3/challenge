package com.automationpractice.enums;

public enum ProductNameEnum {

    BLOUSE("Blouse"),

    FADED_SHORT_T_SHIRTS("Faded Short Sleeve T-shirts"),

    PRINTED_DRESS("Printed Dress"),

    PRINTED_SUMMER_DRESS_1("Printed Summer Dress"),

    PRINTED_CHIFFON_DRESS("Printed Chiffon Dress"),

    PRINTED_SUMMER_DRESS_2("Printed Summer Dress");

    private String value;

    ProductNameEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
