package com.automationpractice.pages.objects;

import com.automationpractice.entities.LoginEntity;
import com.automationpractice.pages.AutomationPracticePage;
import com.automationpractice.utils.ElementUtils;
import com.automationpractice.utils.ReportUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class AuthenticationPo implements AutomationPracticePage {

    private final By AUTHENTICATION_LBL = By.xpath("//h1");

    private final By EMAIL_FLD = By.xpath("//input[@name='email']");

    private final By PASSWORD_FLD = By.xpath("//input[@id='passwd']");

    private final By SIGN_IN_BTN = By.xpath("//button[@id='SubmitLogin']");

    private final By ALERTS_LBL = By.xpath("//div[@class='alert alert-danger']//li");

    public String getAuthenticationLblText() {
        return ElementUtils.getText(AUTHENTICATION_LBL);
    }
    public MyAccountPo doLogin(LoginEntity login) {
        ElementUtils.sendKeys(EMAIL_FLD, 1, login.getEmail());
        ElementUtils.sendKeys(PASSWORD_FLD, login.getPassword());
        ReportUtils.addImage();
        ElementUtils.click(SIGN_IN_BTN);
        return new MyAccountPo();
    }

    public List<String> getAlertLblText() {
        List<String> textList = new ArrayList<>();
        for (WebElement element: ElementUtils.findElements(ALERTS_LBL)) {
            textList.add(ElementUtils.getText(element));
        }
        return textList;
    }
}
