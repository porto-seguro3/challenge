package com.automationpractice.pages.objects;

import com.automationpractice.pages.AutomationPracticePage;
import com.automationpractice.utils.ElementUtils;
import org.openqa.selenium.By;

public class CartPo implements AutomationPracticePage {

    private final By PROCEED_TO_CHECKOUT_BTN = By.xpath("//a/span[text()='Proceed to checkout']");

    public AuthenticationPo clickOnProceedToCheckoutBtn() {
        ElementUtils.click(PROCEED_TO_CHECKOUT_BTN);
        return new AuthenticationPo();
    }
}
