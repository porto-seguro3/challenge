package com.automationpractice.pages.objects;

import com.automationpractice.enums.ProductNameEnum;
import com.automationpractice.pages.AutomationPracticePage;
import com.automationpractice.utils.ElementUtils;
import com.automationpractice.utils.WindowUtils;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HomePo implements AutomationPracticePage {

    private final By CALL_US_NOW_LBL = By.xpath("//span[@class='shop-phone']");

    private final By CONTACT_US_BTN = By.xpath("//a[text() = 'Contact us']");

    private final By SIGN_IN_BTN = By.xpath("//a[contains(text(), 'Sign in')]");

    private final By SEARCH_FLD = By.xpath("//input[@name='search_query']");

    private final By SEARCH_BTN = By.xpath("//button[@name='submit_search']");

    private final By CART_BTN = By.xpath("//a/b[text()='Cart']");

    private final By MENU_WOMEN_BTN = By.xpath("//a[text()='Women']");

    private final By DRESSES_BTN = By.xpath("//ul[contains(@class, 'sf-menu')]/li/a[text()='Dresses']");

    private final By T_SHIRT_BTN = By.xpath("//ul[contains(@class, 'sf-menu')]/li/a[text()='T-shirts']");

    private final By BEST_SELLER_BTN = By.xpath("//li/a[text()='Best Sellers']");

    private final By PRODUCT_NAMES_LBL = By.xpath("//li//h5/a");

    private final By ADD_TO_CART_BTN = By.xpath("//li//a/span[text()='Add to cart']");

    private final By MENU_BLOG_BTN = By.xpath("//a[text()='Blog']");

    private final By RSS_ICN = By.xpath("//li[@class='rss']/a");

    private final By TWITTER_ICN = By.xpath("//li[@class='twitter']/a");

    private final By FACEBOOK_ICN = By.xpath("//li[@class='facebook']/a");

//  Wommen menu items

    private final By WOMEN_TOPS_LBL = By.xpath("//ul[contains(@class, 'submenu-container')]//a[text()='Tops']");

    private final By WOMEN_TSHIRTS_LBL = By.xpath("//ul[contains(@class, 'submenu-container')]//a[text()='T-shirts']");

    private final By WOMEN_BLOUSES_LBL = By.xpath("//ul[contains(@class, 'submenu-container')]//a[text()='Blouses']");

    private final By WOMEN_DRESSES_LBL = By.xpath("//ul[contains(@class, 'submenu-container')]//a[text()='Dresses']");

    private final By WOMEN_CASUAL_DRESSES_LBL = By.xpath("//ul[contains(@class, 'submenu-container')]//a[text()='Dresses'] /..//a[text()='Casual Dresses']");

    private final By WOMEN_EVENING_DRESSES_LBL = By.xpath("//ul[contains(@class, 'submenu-container')]//a[text()='Dresses'] /..//a[text()='Evening Dresses']");

    private final By WOMEN_SUMMER_DRESSES_LBL = By.xpath("//ul[contains(@class, 'submenu-container')]//a[text()='Dresses'] /..//a[text()='Summer Dresses']");

//    Dresses menu items

    private final By DRESSES_CASUAL_DRESSES_LBL = By.xpath("//ul[contains(@class, 'sf-menu')]/li/a[text()='Dresses']/../ul//a[text()='Casual Dresses']");

    private final By DRESSES_EVENING_DRESSES_LBL = By.xpath("//ul[contains(@class, 'sf-menu')]/li/a[text()='Dresses']/../ul//a[text()='Evening Dresses']");

    private final By DRESSES_SUMMER_DRESSES_LBL = By.xpath("//ul[contains(@class, 'sf-menu')]/li/a[text()='Dresses']/../ul//a[text()='Summer Dresses']");

    public AuthenticationPo clickOnSignInBtn() {
        ElementUtils.click(SIGN_IN_BTN, 1);
        return new AuthenticationPo();
    }

    public void clickOnMenuBlogBtn() {
        ElementUtils.click(MENU_BLOG_BTN);
    }

    public void clickOnBestSellerBtn() {
        WindowUtils.scrollTo(BEST_SELLER_BTN);
        ElementUtils.click(BEST_SELLER_BTN);
    }

    public CartPo clickOnCartBtn() {
        ElementUtils.click(CART_BTN);
        return new CartPo();
    }

    public void clickOnRssIcn() {
        WindowUtils.scrollTo(RSS_ICN);
        ElementUtils.click(RSS_ICN);
    }

    public void clickOnTwitterIcn() {
        WindowUtils.scrollTo(TWITTER_ICN);
        ElementUtils.click(TWITTER_ICN);
    }

    public void clickOnFacebookIcn() {
        WindowUtils.scrollTo(FACEBOOK_ICN);
        ElementUtils.click(FACEBOOK_ICN);
    }

    private int getProductNameIndex(ProductNameEnum productNameEnum) {
        List<WebElement> elementList = ElementUtils.findElements(PRODUCT_NAMES_LBL);
        Predicate<WebElement> predicate = element -> ElementUtils.getText(element).equals(productNameEnum.getValue());
        List<WebElement> filtredElementList = elementList.stream().filter(predicate).collect(Collectors.toList());
        switch (productNameEnum) {
            case PRINTED_SUMMER_DRESS_2:
                return elementList.indexOf(filtredElementList.get(1));
            default:
                return elementList.indexOf(filtredElementList.get(0));
        }
    }
    public void addToCart(ProductNameEnum productNameEnum) {
        int index = getProductNameIndex(productNameEnum);
        WebElement productNameLbl = ElementUtils.findElements(PRODUCT_NAMES_LBL).get(index);
        WebElement addToCartBtn = ElementUtils.findElements(ADD_TO_CART_BTN).get(index);
        ElementUtils.hoverAndClick(Arrays.asList(productNameLbl, addToCartBtn));
    }
}
