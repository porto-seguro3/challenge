package com.automationpractice.pages.objects;

import com.automationpractice.pages.AutomationPracticePage;
import com.automationpractice.utils.ElementUtils;
import org.openqa.selenium.By;

public class MyAccountPo implements AutomationPracticePage {

    private final By MY_ACCOUNT_LBL = By.xpath("//h1");

    private final By ACCOUNT_NAME_LBL = By.xpath("//a[@class='account']/span");

    public String getMyAccountLblText() {
        return ElementUtils.getText(MY_ACCOUNT_LBL);
    }

    public String getAccountNameLblText() {
        return ElementUtils.getText(ACCOUNT_NAME_LBL);
    }
}
