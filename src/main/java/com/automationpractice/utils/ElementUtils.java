package com.automationpractice.utils;

import com.automationpractice.config.DriverConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static com.automationpractice.config.DriverConfig.getWebDriver;

public class ElementUtils {

    public static WebElement findElement(By by) {
        return getWebDriver().findElement(by);
    }

    public static List<WebElement> findElements(By by) {
        return getWebDriver().findElements(by);
    }

    public static void waitToBeVisible(WebElement element, long timoutSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(getWebDriver(), Duration.ofSeconds(timoutSeconds));
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }

    private static void waitToBeClickable(WebElement element, long timoutSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(getWebDriver(), Duration.ofSeconds(timoutSeconds));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitToBeVisible(By by, long timoutSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(getWebDriver(), Duration.ofSeconds(timoutSeconds));
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    private static void waitToBeClickable(By by, long timoutSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(getWebDriver(), Duration.ofSeconds(timoutSeconds));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public static void hoverAndClick(List list) {
        if (!list.isEmpty()) {
            Class typeOfList = list.get(0).getClass();
            Actions actions = new Actions(DriverConfig.getWebDriver());
            int sizeOfList = list.size();
            if (typeOfList.equals(RemoteWebElement.class)) {
                for (int i = 0; i < sizeOfList; i++) {
                    actions.moveToElement((WebElement) list.get(i));
                }
                actions.click((WebElement) list.get(sizeOfList-1));
            } else if (typeOfList.equals(By.class)) {
                for (int i = 0; i < sizeOfList-1; i++) {
                    actions.moveToElement(findElement((By) list.get(i)));
                }
                actions.click(findElement((By) list.get(sizeOfList-1)));
            } else {
                throw new IllegalStateException("Unexpected type of list: " + typeOfList);
            }
            actions.build().perform();
        }
    }

    public static void click(WebElement element) {
        element.click();
    }

    public static void clear(WebElement element) {
        element.clear();
    }

    public static void sendKeys(WebElement element, CharSequence... keysToSend) {
        element.sendKeys(keysToSend);
    }

    public static void edit(WebElement element, CharSequence... keysToSend) {
        clear(element);
        sendKeys(element, keysToSend);
    }

    public static String getText(WebElement element) {
        return element.getText();
    }

    public static void click(By by) {
        click(findElement(by));
    }

    public static void clear(By by) {
        clear(findElement(by));
    }

    public static void sendKeys(By by, CharSequence... keysToSend) {
        sendKeys(findElement(by), keysToSend);
    }

    public static void edit(By by, CharSequence... keysToSend) {
        edit(findElement(by), keysToSend);
    }

    public static String getText(By by) {
        return getText(findElement(by));
    }

    public static void click(By by, long secondsOfWait) {
        waitToBeClickable(by, secondsOfWait);
        click(by);
    }

    public static void clear(By by, long secondsOfWait) {
        waitToBeClickable(by, secondsOfWait);
        click(by);
    }

    public static void sendKeys(By by, long secondsOfWait, CharSequence... keysToSend) {
        waitToBeClickable(by, secondsOfWait);
        sendKeys(by, keysToSend);
    }

    public static void edit(By by, long secondsOfWait, CharSequence... keysToSend) {
        waitToBeClickable(by, secondsOfWait);
        edit(by, keysToSend);
    }

    public static String getText(By by, long secondsOfWait) {
        waitToBeVisible(by, secondsOfWait);
        return getText(by);
    }

    public static void click(WebElement element, long secondsOfWait) {
        waitToBeClickable(element, secondsOfWait);
        click(element);
    }

    public static void clear(WebElement element, long secondsOfWait) {
        waitToBeClickable(element, secondsOfWait);
        clear(element);
    }

    public static void sendKeys(WebElement element, long secondsOfWait, CharSequence... keysToSend) {
        waitToBeClickable(element, secondsOfWait);
        sendKeys(element, keysToSend);
    }

    public static void edit(WebElement element, long secondsOfWait, CharSequence... keysToSend) {
        waitToBeClickable(element, secondsOfWait);
        edit(element, keysToSend);
    }

    public static String getText(WebElement element, long secondsOfWait) {
        waitToBeVisible(element, secondsOfWait);
        return getText(element);
    }

}
