package com.automationpractice.utils;

import lombok.SneakyThrows;

import java.io.*;
import java.util.Properties;

public class PropertiesUtils {

    @SneakyThrows
    public static Properties getProperties(File file) {
        Properties properties = new Properties();
        if (!file.exists()) {
            throw new FileNotFoundException(String.format("The file \"%s\" could not be found.", file.getName()));
        }
        properties.load(new FileInputStream(file));
        return properties;
    }
}
