package com.automationpractice.utils;

import io.cucumber.java.Scenario;

public class ReportUtils {

    private static Scenario scenario;

    public static void setScenario(Scenario currentScenario) {
        scenario = currentScenario;
    }

    public static void addImage(String message) {
        scenario.attach(WindowUtils.screenshot(), "image/png", message);
    }

    public static void addImage() {
        addImage("");
    }

    public static void addMessage(String message) {
        scenario.log(message);
    }
}
