package com.automationpractice.utils;

import lombok.SneakyThrows;

import java.util.concurrent.TimeUnit;

public class TimeUtils {

    @SneakyThrows
    public static void wait(int seconds) {
        TimeUnit.SECONDS.sleep(seconds);
    }

}
