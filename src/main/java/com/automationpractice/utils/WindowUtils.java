package com.automationpractice.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import static com.automationpractice.config.DriverConfig.*;

public class WindowUtils {
    public static byte[] screenshot() {
        return ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }

    public static WebDriver switchToNext() {
        var currentWindow = getWebDriver().getWindowHandle();
        for (var window : getWebDriver().getWindowHandles()) {
            if (!currentWindow.contentEquals(window)) {
                return setWebDriver(window);
            }
        }
        return getWebDriver();
    }

    public static WebDriver switchToNew(WindowType windowType) {
        return setWebDriver(windowType);
    }

    public static void scrollTo(WebElement element) {
        new Actions(getWebDriver()).moveToElement(element);
    }

    public static void scrollTo(By by) {
        scrollTo(ElementUtils.findElement(by));
    }

    public static void scrollTo(WebElement element, long secondsTimeout) {
        ElementUtils.waitToBeVisible(element, secondsTimeout);
        scrollTo(element);
    }

    public static void scrollTo(By by, long secondsTimeout) {
        ElementUtils.waitToBeVisible(by, secondsTimeout);
        scrollTo(by);
    }
}
