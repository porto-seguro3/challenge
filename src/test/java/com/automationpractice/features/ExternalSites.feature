# laguage: en
# encoding: utf-8
@ExternalSites
Feature: Acessar sites externos
  Cenários de teste que dão acesso a sites externos

  Background: Acesso ao site
    Given usuário acesse o site

  @ID_80301  @Principal @Pass
  Scenario: Acesso ao blog do site pelo menu da página Home
    When usuário acessa blog do site pelo menu da página Home
    Then usuário é direcionado para o blog

  @ID_80302  @Principal @Pass
  Scenario: Acesso ao blog do site pelo menu da página Home
    When usuário acessa feed do blog do site pelo ícone da página Home
    Then usuário é direcionado para o feed do blog

  @ID_80303  @Principal @Pass
  Scenario: Acesso ao blog do site pelo menu da página Home
    When usuário acessa twitter do blog do site pelo ícone da página Home
    Then usuário é direcionado para o twitter do blog

  @ID_80304  @Principal @Pass
  Scenario: Acesso ao blog do site pelo menu da página Home
    When usuário acessa facebook do blog do site pelo ícone da página Home
    Then usuário é direcionado para o facebook do blog