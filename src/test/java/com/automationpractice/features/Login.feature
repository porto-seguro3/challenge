# laguage; en
# encoding: utf-8
@Login
Feature: Login

  Background: Acesso ao site
    Given usuário acesse o site

  @ID_80201 @Principal @Pass
  Scenario: Fazer login com dados válidos
    And usuário acesse a página Authentication pela página Home
    When usuário fazer login com dados válidos
    Then é validado o redirecionamento do usuário a página My Account

  @ID_80202 @Exception @Pass
  Scenario: Tentar fazer login com endereço de e-mail errado
    And usuário acesse a página Authentication pela página Home
    When usuário tentar fazer login com endereço de e-mail errado
    Then é validada crítica "Authentication failed." na página Authentication

  @ID_80203 @Exception @Pass
  Scenario: Tentar fazer login com senha errada
    And usuário acesse a página Authentication pela página Home
    When usuário tentar fazer login com senha errada
    Then é validada crítica "Authentication failed." na página Authentication

  @ID_80204 @Exception @Pass
  Scenario Outline: Tentar fazer com endereço de e-mail inválido
    And usuário acesse a página Authentication pela página Home
    When usuário tentar fazer login com endereço de e-mail inválido <email>
    Then é validada crítica <critic> na página Authentication
    Examples:
      | email                   | critic                       |
      | ""                      | "An email address required." |
      | "gui.bueuninove.edu.br" | "Invalid email address."     |

  @ID_80205 @Exception @Pass
  Scenario Outline: Tentar fazer com senha inválida
    And usuário acesse a página Authentication pela página Home
    When usuário tentar fazer login com senha inválida <password>
    Then é validada crítica <critic> na página Authentication
    Examples:
      | password | critic                  |
      | ""       | "Password is required." |
      | "gui."   | "Invalid password."     |

  @ID_80206 @Exception @Implementation_Error
  Scenario Outline: Tentar fazer login com endereço de e-mail e senha inválidos
    And usuário acesse a página Authentication pela página Home
    When usuário tentar fazer login com endereço de e-mail <email> e senha inválida <password>
    Then é validada crítica <critic> na página Authentication
    Examples:
      | email                   | password | critic                                                |
      | ""                      | ""       | "An email address required.", "Password is required." |
      | ""                      | "gui."   | "An email address required.", "Invalid password."     |
      | "gui.bueuninove.edu.br" | ""       | "Invalid password.", "Password is required."          |
      | "gui.bueuninove.edu.br" | "gui."   | "Invalid password.", "Invalid password."              |

  @ID_80207 @Alternative @Architecture_Error
  Scenario: Fazer login com dados válidos pela página Shopping-Cart Summary
    And usuário adiciona um produto ao carrinho de compras na página Home
    And usuário acesse a página Authentication pela página Shopping-Cart Summary
    When usuário fazer login com dados válidos
    Then é validado o redirecionamento do usuário a página My Account

