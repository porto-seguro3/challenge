package com.automationpractice.steps;

import com.automationpractice.assets.LoginAssets;
import com.automationpractice.config.DriverConfig;
import com.automationpractice.entities.LoginEntity;
import com.automationpractice.pages.objects.AuthenticationPo;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.List;

public class AuthenticationSteps {

    @When("usuário fazer login com dados válidos")
    public void usuarioFazerLoginComDadosValidos() {
        var authenticationPo = (AuthenticationPo) Hooks.automationPracticePage;
        var login = LoginAssets.getValidLogin();
        Hooks.automationPracticePage = authenticationPo.doLogin(login);
    }

    @When("usuário tentar fazer login com endereço de e-mail errado")
    public void usuarioTentarFazerLoginComEnderecoDeEMailErrado() {
        var authenticationPo = (AuthenticationPo) Hooks.automationPracticePage;
        var login = LoginAssets.getValidLogin();
        login.setEmail("guibue@uninove.edu.br");
        authenticationPo.doLogin(login);
    }

    @When("usuário tentar fazer login com senha errada")
    public void usuarioTentarFazerLoginComSenhaErrada() {
        var authenticationPo = (AuthenticationPo) Hooks.automationPracticePage;
        var login = LoginAssets.getValidLogin();
        login.setPassword("guitester");
        authenticationPo.doLogin(login);
    }

    @When("usuário tentar fazer login com endereço de e-mail inválido {string}")
    public void usuarioTentarFazerLoginComEnderecoDeEMailInvalido(String email) {
        var authenticationPo = (AuthenticationPo) Hooks.automationPracticePage;
        var login = LoginAssets.getValidLogin();
        login.setEmail(email);
        authenticationPo.doLogin(login);
    }

    @When("usuário tentar fazer login com senha inválida {string}")
    public void usuarioTentarFazerLoginComSenhaInvalida(String password) {
        var authenticationPo = (AuthenticationPo) Hooks.automationPracticePage;
        var login = LoginAssets.getValidLogin();
        login.setPassword(password);
        authenticationPo.doLogin(login);
    }

    @When("usuário tentar fazer login com endereço de e-mail {string} e senha inválida {string}")
    public void usuarioTentarFazerLoginComEnderecoDeEMailESenhaInvalida(String email, String password) {
        var authenticationPo = (AuthenticationPo) Hooks.automationPracticePage;
        authenticationPo.doLogin(new LoginEntity(null, email, password));
    }
    
    @Then("é validada crítica {string} na página Authentication")
    public void eValidadaCriticaNaPaginaAuthentication(String alert) {
        var authenticationPo = (AuthenticationPo) Hooks.automationPracticePage;
        String url = DriverConfig.getUrl("automationpractice.authentication");
        List<String> alerts = authenticationPo.getAlertLblText();
        Assert.assertEquals("URL.", url, DriverConfig.getWebDriver().getCurrentUrl());
        Assert.assertEquals("Title.", LoginAssets.AUTHENTICATION_LBL, authenticationPo.getAuthenticationLblText());
        Assert.assertEquals("Number of alerts", 1, alerts.size());
        Assert.assertEquals("Alert.", alert, alerts.get(0));
    }

    @Then("é validada crítica {string}, {string} na página Authentication")
    public void eValidadaCriticaNaPaginaAuthentication(String alert1, String alert2) {
        String[] alerts = {alert1, alert2};
        var authenticationPo = (AuthenticationPo) Hooks.automationPracticePage;
        String url = DriverConfig.getUrl("automationpractice.authentication");
        Assert.assertEquals("URL.", url, DriverConfig.getWebDriver().getCurrentUrl());
        Assert.assertEquals("Title.", LoginAssets.AUTHENTICATION_LBL, authenticationPo.getAuthenticationLblText());
        Assert.assertArrayEquals("Alerts", alerts, authenticationPo.getAlertLblText().toArray());
    }
}
