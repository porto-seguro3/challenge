package com.automationpractice.steps;

import com.automationpractice.pages.objects.CartPo;
import io.cucumber.java.en.Given;

public class CartSteps {

    @Given("usuário acesse a página Authentication pela página Shopping-Cart Summary")
    public void usuarioAcesseAPaginaAuthenticationPelaPaginaShoppingCartSummary() {
        var cartPo = (CartPo) Hooks.automationPracticePage;
        Hooks.automationPracticePage = cartPo.clickOnProceedToCheckoutBtn();
    }
}
