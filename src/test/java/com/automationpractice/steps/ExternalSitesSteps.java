package com.automationpractice.steps;

import com.automationpractice.config.DriverConfig;
import com.automationpractice.utils.WindowUtils;
import io.cucumber.java.en.Then;
import org.junit.Assert;

public class ExternalSitesSteps {

    @Then("usuário é direcionado para o blog")
    public void usuarioEDirecionadoParaOBlog() {
        var webDriver = WindowUtils.switchToNext();
        String urlExpected = DriverConfig.getUrl("blog");
        Assert.assertEquals("URL.", urlExpected, webDriver.getCurrentUrl());
    }

    @Then("usuário é direcionado para o feed do blog")
    public void usuarioEDirecionadoParaOOFeedDoBlog() {
        var webDriver = WindowUtils.switchToNext();
        String urlExpected = DriverConfig.getUrl("blogfeed");
        Assert.assertEquals("URL.", urlExpected, webDriver.getCurrentUrl());
    }

    @Then("usuário é direcionado para o twitter do blog")
    public void usuarioEDirecionadoParaOTwitterDoBlog() {
        var webDriver = WindowUtils.switchToNext();
        String urlExpected = DriverConfig.getUrl("twitter");
        Assert.assertEquals("URL.", urlExpected, webDriver.getCurrentUrl());
    }

    @Then("usuário é direcionado para o facebook do blog")
    public void usuarioEDirecionadoParaOFacebookDoBlog() {
        var webDriver = WindowUtils.switchToNext();
        String urlExpected = DriverConfig.getUrl("facebook");
        Assert.assertEquals("URL.", urlExpected, webDriver.getCurrentUrl());
    }
}
