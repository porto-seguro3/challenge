package com.automationpractice.steps;

import com.automationpractice.config.DriverConfig;
import com.automationpractice.enums.ProductNameEnum;
import com.automationpractice.pages.objects.HomePo;
import com.automationpractice.utils.TimeUtils;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

public class HomeSteps {

    @Given("navegador aberto")
    public void navegadorAberto() {
        DriverConfig.getWebDriver();
    }

    @Given("usuário acesse o site")
    public void usuarioAcesseOSite() {
        WebDriver webDriver = DriverConfig.getWebDriver();
        webDriver.manage().window().maximize();
        webDriver.get(DriverConfig.getUrl("automationpractice"));
    }

    @When("o site é acessado")
    public void oSiteEAcessado() {
        String url = DriverConfig.getUrl("automtionpractice");
        WebDriver webDriver = DriverConfig.getWebDriver();
        webDriver.get(url);
    }

    @Given("usuário acesse a página Authentication pela página Home")
    public void usuarioAcesseAPaginaAuthenticationPelaPaginaHome() {
        Hooks.automationPracticePage = new HomePo().clickOnSignInBtn();
    }

    @When("usuário acessa blog do site pelo menu da página Home")
    public void usuarioAcessaBlogDoSitePeloMenuDaPaginaHome() {
        new HomePo().clickOnMenuBlogBtn();
    }

    @When("usuário acessa feed do blog do site pelo ícone da página Home")
    public void usuarioAcessaFeedDoBlogDoSitePeloIconeDaPaginaHome() {
        new HomePo().clickOnRssIcn();
    }

    @When("usuário acessa twitter do blog do site pelo ícone da página Home")
    public void usuarioAcessaTwitterDoBlogDoSitePeloIconeDaPaginaHome() {
        new HomePo().clickOnTwitterIcn();
    }

    @When("usuário acessa facebook do blog do site pelo ícone da página Home")
    public void usuarioAcessaFacebookDoBlogDoSitePeloIconeDaPaginaHome() {
        new HomePo().clickOnFacebookIcn();
    }

    @Given("usuário adiciona um produto ao carrinho de compras na página Home")
    public void usuarioAdicionaUmProdutoAoCarrinhoDeComprasNaPaginaHome() {
        HomePo homePo = new HomePo();
        homePo.clickOnBestSellerBtn();
        homePo.addToCart(ProductNameEnum.PRINTED_SUMMER_DRESS_2);
        TimeUtils.wait(1);
        Hooks.automationPracticePage = homePo.clickOnCartBtn();
    }
}
