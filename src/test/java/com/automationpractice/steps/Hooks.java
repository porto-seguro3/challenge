package com.automationpractice.steps;

import com.automationpractice.config.DriverConfig;
import com.automationpractice.pages.AutomationPracticePage;
import com.automationpractice.utils.ReportUtils;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;


public class Hooks {

    protected static AutomationPracticePage automationPracticePage;

    @Before
    public void before(Scenario scenario) {
        ReportUtils.setScenario(scenario);
    }

    @AfterStep
    public void afterStep() {
        ReportUtils.addImage();
    }

    @After
    public void after() {
        DriverConfig.closeWebDriver();
    }

}
