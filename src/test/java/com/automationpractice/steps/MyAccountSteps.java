package com.automationpractice.steps;

import com.automationpractice.assets.LoginAssets;
import com.automationpractice.assets.MyAccountAsserts;
import com.automationpractice.config.DriverConfig;
import com.automationpractice.pages.objects.MyAccountPo;
import io.cucumber.java.en.Then;
import org.junit.Assert;

public class MyAccountSteps {

    @Then("é validado o redirecionamento do usuário a página My Account")
    public void eValidadoORedirecionamentoDoUsuarioAPaginaMyAccount() {
        var myAccountPo = (MyAccountPo) Hooks.automationPracticePage;
        var login = LoginAssets.getValidLogin();
        String url = DriverConfig.getUrl("automationpractice.myaccount");
        Assert.assertEquals("URL.", url, DriverConfig.getWebDriver().getCurrentUrl());
        Assert.assertEquals("Title.", MyAccountAsserts.MY_ACCOUNT_LBL, myAccountPo.getMyAccountLblText());
        Assert.assertEquals("Account name.", login.getName(), myAccountPo.getAccountNameLblText());
    }
}
